//
//  main.cpp
//  Matriz-Inversa
//
//  Created by Carlos Paredes on 3/23/20.
//  Copyright © 2020 Paredes. All rights reserved.
//

#include <iostream>
#include "math_tools.h"
#include "display_tools.h"

int main(void) {
    
    Matrix example_matrix, answer_matrix;
    
    zeroes(example_matrix, 3);
    
    example_matrix.at(0).at(0) = 2; example_matrix.at(0).at(1) = 2; example_matrix.at(0).at(2) = 3;
    example_matrix.at(1).at(0) = 4; example_matrix.at(1).at(1) = 5; example_matrix.at(1).at(2) = 6;
    example_matrix.at(2).at(0) = 7; example_matrix.at(2).at(1) = 8; example_matrix.at(2).at(2) = 9;
    
    inverseMatrix(example_matrix, answer_matrix);
    
    showMatrix(answer_matrix);
    return 0;
}
