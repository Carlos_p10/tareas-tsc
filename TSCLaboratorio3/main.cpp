#include <iostream>
#include "math_tools.h"
#include "classes.h"
#include "display_tools.h"
#include "tools.h"
#include "sel.h"

int main() {
    vector<Matrix> localKs;
    vector<Vector> localbs;

    Matrix K;
    Vector b;
    Vector T;

    cout << "*********************************************************************************\n\n"
    << "IMPLEMENTACION DEL METODO DE LOS ELEMENTOS FINITOS\n"
         << "\t- 1 DIMENSION\n"
         << "\t- 2 FUNCIONES DE FORMA LINEALES\n" << "\t- 3 PESOS DE GALERKIN\n\n"
         << "*********************************************************************************\n\n";

    mesh m;
    leerMallayCondiciones(m);

    crearSistemasLocales(m,localKs,localbs);
    zeroes(K,m.getSize(NODES));
    zeroes(b,m.getSize(NODES));
    assembly(m,localKs,localbs,K,b);

    applyNeumann(m,b);

    applyDirichlet(m,K,b);
    zeroes(T,b.size());

    calculate(K,b,T);

    cout << "La respuesta es: " << endl;
    showVector(T);


    return 0;
}
 //open file problem.msh

